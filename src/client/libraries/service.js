setTimeout(() => {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations().then(function (registrations) {

      if (registrations.length > 0) {
        // console.log(`Сервис-воркер уже зарегистрирован`);
      } else {
        navigator.serviceWorker.register('/serverless.js')
          .then(function (registration) {
            console.log(`Сервис-воркер успешно зарегистрирован`, registration);
          })
          .catch(function (error) {
            console.log(`Ошибка регистрации сервис-воркера: ${error}`);
          });
      }
      navigator.serviceWorker.addEventListener('message', function (event) {
        console.log('Сообщение от сервис-воркера:', event.data);
      });
    });

    window.serviceWorkerManager = {
      listServiceWorkers: function () {
        navigator.serviceWorker.getRegistrations().then(function (registrations) {
          console.log('Список зарегистрированных сервис-воркеров:');
          registrations.forEach(function (registration) {
            console.log(registration);
          });
        });
      },

      sendMessageToServiceWorker: function (message) {
        navigator.serviceWorker.controller.postMessage(message);
        broadcast.postMessage({
          payload: message,
          type: 'MESSAGE',
        });
    
      },

      unregisterServiceWorker: function () {
        navigator.serviceWorker.getRegistrations().then(function (registrations) {
          if (registrations.length > 0) {
            registrations[0].unregister().then(function (success) {
              if (success) {
                console.log('Сервис-воркер успешно удален');
              }
            });
          }
        });
      }
    };
  
    const broadcast = new BroadcastChannel('count-channel');
    broadcast.onmessage = (event) => {
      console.log(event.data.payload);
    };
    broadcast.postMessage({
      type: 'INCREASE_COUNT',
    });
  }
}, 2000)