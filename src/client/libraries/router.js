export default class Router {
  constructor() {
    this.routes = [];
  }

  addRoute(pattern, callback) {
    const segments = pattern.split('/').filter(Boolean);
    const keys = [];
    const regexPattern = segments.map(segment => {
      if (segment.startsWith(':')) {
        keys.push(segment.slice(1));
        return '([^/]+)';
      }
      return segment;
    }).join('/');

    const regex = new RegExp(`^${regexPattern}$`);

    this.routes.push({
      regex,
      keys,
      callback,
    });
  }

  match(url) {
    const pathSegments = url.split('/').filter(Boolean);

    for (const route of this.routes) {
      const match = pathSegments.join('/').match(route.regex);
      if (match) {
        const params = {};
        route.keys.forEach((key, index) => {
          params[key] = match[index + 1];
        });
        return { params, callback: route.callback };
      }
    }

    return null;
  }
}