import React, { useState } from 'react';
import { SEO } from './../components/seo';

export const AlbumsLayout = (props) => {
  const { title, menu, content, meta, base } = props;

  const SEOBlock = <SEO meta={meta} />

  const menuBlock = menu.map((item, index) => {
    if(index < menu.length - 1)
    return (
      <><a key={index} href={item.href}>{item.title}</a><span>{" > "}</span></>
    )
    else return (
      <b>{item.title}</b>
    )
  })

  const albums = content.map((album, index) => {
    return (
      <>
        <article key={index}>{'\n'}
          <h2><a href={`/albums/${album.id}`}>{album.title}</a></h2>{'\n'}
          <p>{album.body}</p>{'\n'}
          <p><a href={`/albums/${album.id}`}>View more</a></p>{'\n'}
        </article>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {SEOBlock}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body className='albums'>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          {menuBlock}
        </nav>
        <main>
          <div id="albums" data-content={btoa(JSON.stringify(content))}>
            {albums}
          </div>
        </main>
        <footer>
          <p>© 2023</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export default AlbumsLayout;