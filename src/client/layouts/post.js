import React, { useState } from 'react';
import { SEO } from './../components/seo';

export const PostLayout = (props) => {
  const { title, menu, content, meta, base } = props;

  const SEOBlock = <SEO meta={meta} />

  const menuBlock = menu.map((item, index) => {
    if(index < menu.length - 1)
    return (
      <><a key={index} href={item.href}>{item.title}</a><span>{" > "}</span></>
    )
    else return (
      <b>{item.title}</b>
    )
  })

  const comments = content.commentsData.map((comment, index) => {
    return (
      <>
        <div key={index}>{'\n'}
          <h4><a href={`mailto:${comment.email}`}>{comment.name}</a></h4>{'\n'}
          <p>{comment.body}</p>{'\n'}
          <p><a href={`mailto:${comment.email}`}>{comment.email}</a></p>{'\n'}
        </div>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {SEOBlock}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body className='post'>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          {menuBlock}
        </nav>
        <main>
          <div id="post" data-content={btoa(JSON.stringify(content))}>
            <h2>{content.postData.title}</h2>
            <p>{content.postData.body}</p>
            <h3>Comments:</h3>
            {comments}
          </div>
        </main>
        <footer>
          <p>© 2023</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export default PostLayout;