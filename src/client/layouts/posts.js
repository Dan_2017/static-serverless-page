import React, { useState } from 'react';
import { SEO } from './../components/seo';

export const PostsLayout = (props) => {
  const { title, menu, content, meta, base } = props;

  const SEOBlock = <SEO meta={meta} />

  const menuBlock = menu.map((item, index) => {
    if(index < menu.length - 1)
    return (
      <><a key={index} href={item.href}>{item.title}</a><span>{" > "}</span></>
    )
    else return (
      <b>{item.title}</b>
    )
  })

  const posts = content.map((post, index) => {
    return (
      <>
        <article key={index}>{'\n'}
          <h2><a href={`/posts/${post.id}`}>{post.title}</a></h2>{'\n'}
          <p>{post.body}</p>{'\n'}
          <p><a href={`/posts/${post.id}`}>Read more</a></p>{'\n'}
        </article>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {SEOBlock}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body className='posts'>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          {menuBlock}
          {/* <a href="/">Home</a> |
          <a href="/page/1">Page 1</a> |
          <a href="/page/100">Page 100</a> */}
        </nav>
        <main>
          <div id="posts" data-content={btoa(JSON.stringify(content))}>
            {posts}
          </div>
        </main>
        <footer>
          <p>© 2023</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export default PostsLayout;