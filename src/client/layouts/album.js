import React, { useState } from 'react';
import { SEO } from './../components/seo';

export const AlbumLayout = (props) => {
  const { title, menu, content, meta, base } = props;

  const SEOBlock = <SEO meta={meta} />

  const menuBlock = menu.map((item, index) => {
    if(index < menu.length - 1)
    return (
      <><a key={index} href={item.href}>{item.title}</a><span>{" > "}</span></>
    )
    else return (
      <b>{item.title}</b>
    )
  })

  const photos = content.photosData.map((photo, index) => {
    return (
      <>
        <div key={index}>{'\n'}
          <h4>{photo.title}</h4>{'\n'}
          <p><img src={photo.url} /></p>{'\n'}
        </div>{'\n'}
      </>
    )
  })

  return (
    <html>
      <head>
        <title>{title}</title>
        <meta charSet="UTF-8" />
        {SEOBlock}
        <link rel="icon" type="image/ico" href="/favicon.ico" id="favicon"></link>
        <base href={base}></base>
      </head>
      <body className='album'>
        <header>
          <h1>{title}</h1>
        </header>
        <nav>
          {menuBlock}
        </nav>
        <main>
          <div id="album" data-content={btoa(JSON.stringify(content))}>
            <h2>{content.albumData.title}</h2>
            <h3>Photos:</h3>
            {photos}
          </div>
        </main>
        <footer>
          <p>© 2023</p>
        </footer>
        <script src="/index.js"></script>
      </body>
    </html>
  );
};

export default AlbumLayout;