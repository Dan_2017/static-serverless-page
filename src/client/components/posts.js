import React from 'react';

const PostsList = ({ content }) => {
  const _content = JSON.parse(atob(content))

  const posts = _content.map((post, index) => {
    return (
      <>
        <article key={index}>{'\n'}
          <h2><a href={`/posts/${post.id}`}>{post.title}</a></h2>{'\n'}
          <p>{post.body}</p>{'\n'}
          <p><a href={`/posts/${post.id}`}>Read more</a></p>{'\n'}
        </article>{'\n'}
      </>
    )
  })

  return (
    <div>
      {posts}
    </div>
  );
};

export default PostsList;