import React, { useState, useEffect } from 'react';

const UsersList = ({ param1, param2 }) => {
  const [users, setUsers] = useState([])
  const [searchResults, setSearchResults] = useState([])
  const [search, setSearch] = useState('')
  const [sortDir, setSortDir] = useState(false)

  useEffect(async () => {
    const fetchUsers = async () => {
      const response = await fetch(`https://jsonplaceholder.typicode.com/users`)
      const json = await response.json()
      setUsers(json)
      setSearchResults(json)
    }
    fetchUsers();
  }, [])

  const applySearchAndSort = () => {
    let filteredUsers = [...users];

    if (search) {
      const searchRe = new RegExp(`(${search})`, 'g');
      filteredUsers = filteredUsers.filter((user) => searchRe.test(user.name));
    }

    filteredUsers.sort((a, b) => {
      const nameA = a.name.toLowerCase();
      const nameB = b.name.toLowerCase();
      if (nameA < nameB) return sortDir ? -1 : 1;
      if (nameA > nameB) return sortDir ? 1 : -1;
      return 0;
    });

    setSearchResults(filteredUsers);
  };

  useEffect(applySearchAndSort, [search, sortDir])

  const handleSearch = (e) => {
    setSearch(e.target.value)
  }

  const handleSort = (e) => {
    setSortDir(!sortDir)
  }

  const usersList = searchResults.map((user, index) => {
    let userName = user.name
    let nameParts = []
    if (search) {
      const searchRe = new RegExp(`(${search})`, "g");
      nameParts = user.name.split(searchRe);
      userName = nameParts.map((part, i) => {
        if (i % 2 === 1) {
          return <i key={i}>{part}</i>;
        } else {
          return part;
        }
      });
    }

    if (nameParts.length > 1 && search || !search) return (
      <li key={user.id}>
        <b className='user-name'>{userName}</b>
        <a href={`/user/${user.id}/posts`}>posts</a>
        <a href={`/user/${user.id}/albums`}>albums</a>
      </li>
    );
  });

  return (
    <div className='users-list'>
      <h2>Users</h2>
      <div className='actions'>
        <div className='sortField'>
          <span className='fieldTitle'>NAME</span>
          <span className={`sort ${sortDir ? 'asc' : 'desc'}`} onClick={handleSort}>
            <svg className="sortLabel" focusable="false" aria-hidden="true" viewBox="0 0 24 24" width="20px" height="20px"><path d="m7 10 5 5 5-5z"></path></svg>
          </span>
        </div>
        <div className='searchField'>
          <svg class="searchIcon" focusable="false" aria-hidden="true" viewBox="0 0 24 24" width="20px" height="20px"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
          <input className="search" type='text' value={search} onChange={handleSearch}></input>
        </div>
      </div>
      <ul>{usersList}</ul>
      <p className='results'>{searchResults.length > 0 ? `Results: ${searchResults.length}` : `No results`}</p>
    </div>
  );
};

export default UsersList;