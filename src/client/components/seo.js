import React from 'react';
import ReactDOMServer from 'react-dom/server';

export const SEO = (props) => {
  const { title, content, meta, base } = props;
  
  const metaDefaults = [
    { name: "viewport", content: "width=device-width, initial-scale=1, shrink-to-fit=no" },
  ];

  const metaTags = [...metaDefaults, ...meta].map((m, i) => (
    <><meta name={m.name} content={m.content} key={i} />{i === [...metaDefaults, ...meta].length - 1 ? "" : "\n    "}</>
  ));

  return (
    <>{metaTags}</>
  )
}

// const metas = [
//   { name: "viewport", content: "width=device-width, initial-scale=1.0" },
//   { name: "title", content: "title" },
//   { name: "description", content: "description" }
// ];

// const metaTags = metas.map((m, i) => (
//   <meta name={m.name} content={m.content} key={i} />
// ));

// const test = ReactDOMServer.renderToString(<div>YYYYYYYYY123YYYYYYY</div>);
// const test = ReactDOMServer.renderToString(<>{metaTags}</>);

const SEOTags = (_m) => {
  return ReactDOMServer.renderToString(<SEO meta={_m ? _m : []} />)
}

export default SEOTags;